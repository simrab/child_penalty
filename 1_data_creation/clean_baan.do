/******************** Child Penalty Project *********************

** Do_file: clean_baan (largely inspired by "H:\child_penalty\do\dofiles_Sara\employment_loop_yearly.do")
	* Author: SiRa (SaRe) (changes marked by modif SR)
	* Creation: 10/03/2020
	* Last update: 10/03/2020
	
** Outline of the do-file: 
I.   Initialisation 
II.  Data creation
****************************************************************/


*****************  I. Initialisation ********************


*** Check in globals defined (from master.do)
if missing("$path_data_CP"){
BREAK 
}

if missing("$path_tmp_data"){
BREAK 
}


*****************  II. Data creation ********************


******  II.1 Import data and select id *******


forvalues k=1999/2016 {
	foreach dataset in "BAANKENMERKENBUS" "BAANSOMMENTAB" {

	if "`dataset'" == "BAANKENMERKENBUS" {
	global save_name     "$path_tmp_data\baankenmerkenbus`k'.dta"
	}
	if "`dataset'" == "BAANSOMMENTAB" {
	global save_name     "$path_tmp_data\baansommentab`k'.dta"
	}
	
	if `k' < 2016 {
	global path_data  "G:\Arbeid\\`dataset'\\`k'\geconverteerde data\"
	}
	else {
	global path_data  "G:\Arbeid\\`dataset'\geconverteerde data\"
	}
	cd "$path_data"
	

	** Find latest version 
	local l=1
	fs *.dta
	foreach f in `r(files)' {
		if strpos("`f'", "`k'"){
			local v`l'=substr("`f'",-5,1)
			*di `v`l'' 
			if `l'>1 {
				local j=`l'-1 
				local v_latest=cond(`v`l''> `v`j'',`v`l'',`v`j'')
			}
			else {
				local v_latest=`v`l''
			}
			
			local l=`l'+1
		}
	}


	** Load data
	di "`v_latest'"
	fs *`v_latest'.dta
	foreach f in `r(files)' {
	use "`f'", clear
	}
	
	if missing(`v_latest'){
	di "No baan file found for year `k'"
	BREAK	
	}
	
	
	** Merge with population
	merge m:1 rinpersoons rinpersoon using "$path_data_CP\population.dta", ///
	keep(match) nogen keepusing(rinpersoons rinpersoon)
		
	** Save
	save "$save_name", replace
	
}
}

*****  II.2  Panel creation *****


set more off
global final="$path_data_CP"

forvalues k=1999/2016 {

	** Individuals in year k

	use "$path_data_CP\panel_population.dta", clear
	keep if year == `k'
	keep rinpersoons rinpersoon
	save "$path_tmp_data\tmp_pop2.dta", replace
	

    *** A) Baankenmerkenbus database

	** Load data
	use "$path_tmp_data\baankenmerkenbus`k'", clear

	** Keep observations for the selected sample
	merge m:1 rinpersoons rinpersoon using "$path_tmp_data\tmp_pop2.dta", keep(match using) nogen
	sort rinpersoons rinpersoon 
	
	** keep 1 entry per id x baanid
	sort rinpersoons rinpersoon baanid aanvangbaanid
	bys rinpersoons rinpersoon baanid: gen id=_n
	by rinpersoons rinpersoon baanid: replace ///
	eindebaanid=eindebaanid[_N] if id==1
	keep if id==1
	compress
	
	** Select variables
	keep   rinpersoon  rinpersoons sectbaanid aanvangbaanid eindebaanid ///
	caosectorbaanid beidbaanid baanid

	** Save dataset
	save "$path_tmp_data\tmp_baan.dta", replace
	
	
	 *** B) baansommentab database
	** Load data
	use "$path_tmp_data\baansommentab`k'", clear
	
	** Keep observations for the selected sample
	merge m:1 rinpersoons rinpersoon using "$path_tmp_data\tmp_pop2.dta", keep(match using) nogen
	sort rinpersoons rinpersoon 
	
	** Variables selection
	keep rinpersoons rinpersoon baanid blsv fiscloon kaldg deeltijdfactorbaanid

	** keep 1 entry per id x baanid
	bys rinpersoons rinpersoon baanid: keep if _n == _N
		
	
	*** C) Merge baansommentab Baankenmerkenbus
	merge 1:1 rinpersoon rinpersoons baanid using "$path_tmp_data\tmp_baan.dta", assert(match) nogen
	
	
	
	*** Variables creation
	gen year = `k'
	rename fiscloon fiscal_wage 
	rename blsv gross_wage 
	rename deeltijdfactorbaanid part_time_factor
	rename sectbaanid sect
	
	gen caosect = substr(caosectorbaanid, 1, 1)

	
	***  Imputation of hours worked
	merge m:1 sect year using "$path_data_CP\work_duration.dta", nogen keep(match)
	gen share_year_employed = kaldg/365
	gen annual_hours = mode_work_duration*12
	gen hours_worked = annual_hours*part_time_factor*share_year_employed

	** Keep 1 entry per id (sum for hours and wage, average for partime, main baan for sect)
	gen duration =  date(eindebaanid, "YMD") - date(aanvangbaanid, "YMD")
	sort rinpersoons rinpersoon duration

	destring sect, replace force
	destring caosect, replace force
	gcollapse (sum) hours_worked gross_wage fiscal_wage (mean) part_time_factor (last) sect caosect year, by (rinpersoons rinpersoon)
	
	* MV to 0
	replace gross_wage = 0 if gross_wage == .
	rename gross_wage earnings_baan1
	replace fiscal_wage = 0 if fiscal_wage == .
	rename fiscal_wage earnings_baan2
	replace hours_worked = 0 if hours_worked == . 
	
	* Outliers
	
	** Save
	drop if rinpersoon  == ""
	compress * 
	if  `k' > 1999 {
		append using "$path_data_CP\baan_panel.dta"
		}
	save "$path_data_CP\baan_panel.dta", replace	
}		
		

* Erase temporary files		
forvalues k=1999/2016 {
erase "$path_tmp_data\\baankenmerkenbus`k'.dta"	
erase "$path_tmp_data\\baansommentab`k'.dta"	
}
erase "$path_tmp_data\\tmp_pop2.dta"	
erase "$path_tmp_data\\tmp_baan.dta"		
	
	

** Descriptive statistics 
gen obs = 1
gen participation1 = earnings_baan1 > 0 
gen participation2 = earnings_baan2 > 0 
gcollapse (sum) obs (mean) earnings* participation* part_time_factor av_part_time, by(year)
save  "H:\child_penalty\output\data\desc_baan.dta", replace


