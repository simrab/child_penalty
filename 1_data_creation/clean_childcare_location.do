/******************** Child Penalty Project *********************

** Do_file: clean_hh: clean childcare facilities
	* Author: Simon Rabate 
	* Creation: 11/03/2020
	* Last update: 11/03/2020
	
** Goal of the dofile: 
For each year, compute the number of childcare related job by cities.
Merge between
- Job locations (GEMSTPLAATSBUS) and followers
- SBI number (BETAB)
- Using BAANKENMERKENBUS and SPOLIS for the link between beid and job id.

WARNING: 
For some reason, the match between GEMSTPLAATSBUS and BAANKENMERKENBUS only works with the V2
versions of the BAANKENMERKENBUS datasets. 
	
** Outline of the do-file: 
I.   Initialisation 
II.  Data creation
****************************************************************/



*****************  I. Initialisation ********************


*** Check in globals defined (from master.do)
if missing("$path_data_CP"){
BREAK1
}

if missing("$path_tmp_data"){
BREAK2 
}

*****************  II. Data cleaning ********************



*** II.1 Cleaning BAANKENMERKENBUS ********************

forvalues k=1999/2009 {

	global k `k'
	global save_name     "$path_tmp_data\job_beid$k.dta"
	
	global path_data  "G:\Arbeid\\BAANKENMERKENBUS\\$k\geconverteerde data\"
	cd "$path_data"
	
	global version 2
	if $k == 2007 {
	global version 1
	}

	** Select version
	global df 
	fs *.dta
	foreach f in `r(files)' {
		if strpos("`f'", "`k'"){
			local v=substr("`f'",-5,1)
			if  `v' == $version {
			global df "`f'"
			}
		}
	}	
		
		
	if missing("$df"){
	di "No 'V2 baan file found for year `k'"
	BREAK	
	}	

	** Load data
	di "$df"
	use "$df", clear

	
	* Keep only longer spell (some baanid have more than one obs)
	gen duration = date(eindebaanid, "YMD") - date(aanvangbaanid, "YMD")
	bys rinpersoons rinpersoon baanid (duration): keep if _n == _N
	keep rinpersoons rinpersoon baanid beidbaanid
	rename beidbaanid beid
		
	** Save
	save "$save_name", replace
	
}



*** II.2 Cleaning SPOLIS ********************

global path_data_spolis "G:\Spolis\SPOLISBUS\"
forvalues k=2010/2014 {
	global save_name     "$path_tmp_data\job_beid`k'.dta"

	global k `k'

	di "Cleaning spolis data for $k " 
	cd "$path_data_spolis\\$k\geconverteerde data\"
	** Find latest version 
	local l=1
	fs *.dta
	foreach f in `r(files)' {
		if strpos("`f'", "new"){ 
		di "aa"
			local v_latest= "new"
		} 
		else if strpos("`f'", "$k"){
			local v`l'=substr("`f'",-5,1)
			di `v`l'' 
			if `l'>1 {
				local j=`l'-1 
				local v_latest=cond(`v`l''> `v`j'',`v`l'',`v`j'')
			}
			else {
				local v_latest=`v`l''
			}
			
			local l=`l'+1
		}
	}

	
	** Load data
	global list_var "RINPERSOON RINPERSOONS IKVID SBEID"
	if inrange($k, 2013, 2016){
	global list_var "rinpersoon rinpersoons ikvid sbeid"
	}
	di "`v_latest'"
	fs *`v_latest'.dta
	di `r(files)'
	foreach f in `r(files)' {
	use  $list_var using "`f'", clear
	}
	
	rename  *,  lower
	bys rinpersoons rinpersoon ikvid: keep if _n == 1
	
	rename ikvid baanid
	rename sbeid beid
	compress *
	save "$save_name", replace
}


*** II.3 Cleaning GEMSTPLAATSBUS ********************

** 1999-2014: GEMSTPLAATSBUS and GEMSTPLBUS
forvalues k=1999/2014 {
	global k `k'

	global save_name     "$path_tmp_data\baan_location$k.dta"
	
	** GEMSTPLAATSBUS 1999-2005
	global path_data  "G:\Arbeid\GEMSTPLAATSBUS\\$k\geconverteerde data\"
	global baanvar "baanid"
	
	** GEMSTPLBUS 2006-2014
	if $k > 2005 {
		global path_data  "G:\Arbeid\GEMSTPLBUS\\$k\geconverteerde data\"
		global baanvar "baanrugid"
		if $k > 2009 {
			global baanvar "ikvid"
		}
	}
	
	** GEMSTPLBUS 2015-2016
	if $k > 2014 {
	global path_data  "G:\Arbeid\GEMEENTESTPLTAB\geconverteerde data\"
	global baanvar "ikvid"
	}

	cd "$path_data"

	
	** Find latest version 
	local l=1
	fs *.dta
	foreach f in `r(files)' {
		if strpos("`f'", "$k"){
			local v`l'=substr("`f'",-5,1)
			*di `v`l'' 
			if `l'>1 {
				local j=`l'-1 
				local v_latest=cond(`v`l''> `v`j'',`v`l'',`v`j'')
			}
			else {
				local v_latest=`v`l''
			}
			
			local l=`l'+1
		}
	}


	** Load data
	di "`v_latest'"
	fs *`v_latest'.dta
	foreach f in `r(files)' {
	use "`f'", clear
	}
	
	if missing(`v_latest'){
	di "No fir, location file found for year `k'"
	BREAK	
	}
	
	** Clean data
	* Variable name
	rename *, lower
	if inrange($k,2010,2014) {
	rename n* *
	rename eind datumeinde
	rename aanv datumaanvang
	}
	rename $baanvar baanid
	rename gem gemeentecode
	
	* Keep only longer spell (some baanid have more than one obs)
	if $k <= 2014 {
	gen duration = date(datumeinde, "YMD") - date(datumaanvang, "YMD")
	bys rinpersoons rinpersoon baanid (duration): keep if _n == _N
	}
	else {
	bys rinpersoons rinpersoon baanid: keep if _n == 1
	}
	
	keep rin* baanid gemeentecode
	** Save
	compress *
	save "$save_name", replace	

}

*****************  III. Panel creation ********************


forvalues k=1999/2014 {

	global k `k'

	clear
	** 1. Load employment spells
	 use "$path_tmp_data\job_beid$k.dta"

	** 2. Merge with SBI using betab
	cd "G:\Arbeid\BETAB\\$k\geconverteerde data\"
	fs *.dta
	foreach f in `r(files)' {
	global betab `f'
	}
	merge m:1 beid using "$betab", keep(match) nogen keepusing(SBI1993 SBI2008)

	** 3. Identifiying childcare 
	* Correspondance file:///K:\Utilities\Code_Listings\Bedrijfsindelingen\schakelschemasbi93sbi2008versie732012.xls
	*85331	88911	Kinderopvang
	*	    88912	Peuterspeelzaalwerk
	rename *, lower
	gen childcare_baan_sbi93  = sbi1993 == "85331"
	gen childcare_baan_sbi08a = sbi2008 == "88911"
	gen childcare_baan_sbi08b = sbi2008 == "88912"
		
	** 4. Merge with localisation data
	 merge 1:1 rinpersoons rinpersoon baanid using  "$path_tmp_data\baan_location$k.dta"
	 drop if _merge == 2
	 replace gemeentecode = "not_matched" if _merge == 1

	** 5.Total number by gemeente
	gen jobs = 1
	gcollapse (sum) jobs childcare_baan_sbi93 childcare_baan_sbi08a childcare_baan_sbi08b, by(gemeentecode)

	** 6. 	Save data
	gen year = $k
	
	if ($k > 1999) {
	append using "$path_data_CP\panel_childcare_jobs.dta"
	}
	save "$path_data_CP\panel_childcare_jobs.dta", replace
	
}


* Erase temporary files		
forvalues k=1999/2014 {
erase "$path_tmp_data\job_beid`k'.dta"
erase "$path_tmp_data\baan_location`k'.dta"
}